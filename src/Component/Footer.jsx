import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div
        style={{ height: "100px", lineHeight: "100px" }}
        className="bg-dark text-white"
      >
        Copyright Footer
      </div>
    );
  }
}
