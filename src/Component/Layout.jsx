import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Item from "./Item";
import Item2 from "./Item2";

export default class Layout extends Component {
  render() {
    return (
      <div className="">
        <Header />
        <div className="container pt-3">
          <Banner />
          <div className="d-flex py-3">
            <Item />
            <Item />
            <Item />
            <Item />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
