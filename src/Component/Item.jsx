import React, { Component } from "react";
import "./Card.css";

export default class Item extends Component {
  render() {
    return (
      <div className="wrapper">
        <div className="card ">
          <div className="card-top">500 X 325</div>
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <button className="btn btn-primary">Find out more!</button>
          </div>
        </div>
      </div>
    );
  }
}
