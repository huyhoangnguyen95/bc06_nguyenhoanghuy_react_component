import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div>
        <div className="bg-secondary p-5 text-left">
          <h2>A Warm Welcome</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus
            qui, incidunt cum porro adipisci voluptatem ipsam debitis sunt
            repudiandae error?
          </p>
          <button className="btn btn-primary">Call to action!</button>
        </div>
      </div>
    );
  }
}
